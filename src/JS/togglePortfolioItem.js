import { portfolioList as portfolio } from './selectors';

function expandPortfolioItem(e) {
    e.target.closest('.portfolio__item') &&
        e.target.closest('.portfolio__item').classList.add('portfolio__item_active');
}

function hidePortfolioItem(e) {
    e.relatedTarget instanceof Element &&
        e.target.closest('.portfolio__item') &&
        !e.relatedTarget.closest('.portfolio__item') &&
        e.target.closest('.portfolio__item').classList.remove('portfolio__item_active');
}

portfolio.addEventListener('mouseover', e => expandPortfolioItem(e));
portfolio.addEventListener('mouseout', e => hidePortfolioItem(e));