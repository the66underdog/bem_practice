import {
    sliderList as slider,
    sliderController as controllers,
    sliderNumeration as numeration
} from "./selectors";

let currentSlide = 0;
const nums = slider.children.length;

function toggleNumeration() {
    for (let item of numeration.children) {
        (item.dataset.num === `${currentSlide}`) ? item.classList.add('slider__number_current') :
            item.classList.remove('slider__number_current');
    }
}

function defineDirection(dir = 1) {
    if ((dir === 1 && currentSlide !== nums - 1) || (dir === -1 && currentSlide !== 0)) {
        currentSlide += dir;
        slider.scrollTo(currentSlide * slider.clientWidth, 0);
        toggleNumeration();
    }
}

function generateNumerationItem(counter) {
    const numItem = document.createElement("li");
    numItem.classList.add('slider__number');
    numItem.dataset.num = `${counter}`;
    (counter < 9) ? numItem.innerHTML = `0${counter + 1}` :
        numItem.innerHTML = `${counter + 1}`;
    numeration.appendChild(numItem);
}

function countNums() {
    for (let i = 0; i < slider.children.length; i++) {
        generateNumerationItem(i);
    }
    toggleNumeration();
}

controllers[0].addEventListener('click', () => defineDirection(-1));
controllers[1].addEventListener('click', () => defineDirection());
window.addEventListener('resize', function () {
    slider.scrollTo({
        left: currentSlide * slider.clientWidth,
        behavior: 'instant'
    });
});
window.onload = () => {
    countNums();
}